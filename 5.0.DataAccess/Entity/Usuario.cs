﻿
using _5._0.DataAccess.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _5._0.DataAccess.Entity
{
    [Table("tusuario")]
    public class Usuario:EntityGeneric
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string idUsuario { get; set; }
        public string idContraseña { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Dni{ get; set; }
        public string NumeroTelefonico { get; set; }
        public string Direccion { get; set; }
        public string EmailUsuario { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string CodigoPostal { get; set;}
       
        [InverseProperty("parentUsuario")]
        public List<Contacto> childContacto { get; set; }

        [InverseProperty("parentUsuario")]
        public List<Actividad> childActividades { get; set; }
    }
}
