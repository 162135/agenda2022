﻿using _5._0.DataAccess.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace _5._0.DataAccess.Entity
{
    [Table("tcontacto")]
    public class Contacto :EntityGeneric
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string idContacto { get; set; }
        public string idUsuario { get; set; }
        public string NombreContacto { get; set; }
        public string NumeroContacto { get; set; }
        public string EmailContacto { get; set; }
        public string NotasContacto { get; set; }
          [ForeignKey(nameof(idUsuario))]
        public Usuario parentUsuario { get; set; }
    }
}
