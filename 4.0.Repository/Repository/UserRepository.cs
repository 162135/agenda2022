﻿using _0._0.DataTransfer.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4._0.Repository.Repository
{
    public  interface UserRepository
    {
        
        public bool ExistsByDni(string Dni);

        public int Insert(DtoUsuario dto);
        public int Delete(string idService);
        public int Edit(DtoUsuario dto);

        public List<DtoUsuario> GetAll();
    }
}
