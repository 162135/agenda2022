using _0._0.DataTransfer.DtoAdditional;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);
DtoAppSettings.originRequest = builder.Configuration.GetSection("GlobalSeting").GetSection("originRequest").Value;
DtoAppSettings.jwtSecret = builder.Configuration.GetSection("GlobalSetting").GetSection("jwtSecret").Value;
builder.Services.Configure<ApiBehaviorOptions>(Options => {
    Options.SuppressModelStateInvalidFilter = true;
});
// Add services to the container.
builder.Services.AddCors(options =>
{
 
    options.AddPolicy("cors",
        policy =>
        {
            policy.WithOrigins("http://localhost:4200")
                                .AllowAnyHeader()
                                .AllowAnyMethod();
        });
});
//implementacion para el usos de JWT
builder.Services.AddAuthentication(options => {
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(jwrBearerOptions=>{
    jwrBearerOptions.TokenValidationParameters = new TokenValidationParameters() {
        ValidateIssuer = false,
        ValidateActor = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ValidAudience = "API_REST",
        ClockSkew = TimeSpan.Zero,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(DtoAppSettings.jwtSecret))
    };
    }); 



builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();





DtoAppSettings.ConnectionStringSqlServer = builder.Configuration.GetValue<string>("ConnectionStrings:ConnectionStringSqlServer");

builder.Services.AddControllers().AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{app.UseAuthentication();
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors();

app.UseAuthorization();

app.MapControllers();

app.Run();
