﻿using _0._0.DataTransfer.Dto;
using _0._0.DataTransfer.DtoAdditional;
using _3._0.Business.Generic;


namespace _3._0.Business.Business.Usuario
{
    public partial class BusinessUser:BusinessGeneric
    {
        public DtoMessage Insert(DtoUsuario dto) {
            dto.idUsuario = Guid.NewGuid().ToString();
            dto.FechaRegistro = DateTime.Now;
            dto.FechaModificacion = DateTime.Now;

            ValidationInsert(dto);

            if (_mo.existsMessage())
            {
                return _mo;
            }

            _repoService.Insert(dto);

            _mo.listMessage.Add("Operación realizada correctamente.");
            _mo.success();

            return _mo;

        
        }
        public (DtoMessage, List<DtoUsuario>) GetAll()
        {
            List<DtoUsuario> listDtoService = _repoService.GetAll();

            _mo.success();

            return (_mo, listDtoService);
        }

        public DtoMessage Delete(string idService)
        {
            if (_repoService.Delete(idService) == 0)
            {
                _mo.listMessage.Add("No se encontró registro para eliminar.");
                _mo.warning();
            }
            else
            {
                _mo.listMessage.Add("El registro fue eliminado correctamente.");
                _mo.success();
            }

            return _mo;
        }

        public DtoMessage Edit(DtoUsuario dto)
        {
          /*  dto.modificationDate = DateTime.Now;

            ValidationEdit(dto);

            if (_mo.existsMessage())
            {
                return _mo;
            }*/

            _repoService.Edit(dto);

            _mo.listMessage.Add("Operación realizada correctamente.");
            _mo.success();

            return _mo;
        }
    }
}

