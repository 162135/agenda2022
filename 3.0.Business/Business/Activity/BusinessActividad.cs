﻿using _0._0.DataTransfer.Dto;
using _0._0.DataTransfer.DtoAdditional;
using _3._0.Business.Generic;


namespace _3._0.Business.Business.Usuario
{
    public partial class BusinessActividad:BusinessGeneric
    {
        public DtoMessage Insert(DtoActividades dto)
        {
            dto.idActividad = Guid.NewGuid().ToString();
            dto.FechaRegistro = DateTime.Now;
            dto.FechaModificacion = DateTime.Now;
            _repoService.Insert(dto);

            _mo.listMessage.Add("Operación realizada correctamente.");
            _mo.success();

            return _mo;


        }
        public List<DtoActividades> getAll()
        {
            return _repoService.getAll();


        }
    }
}
